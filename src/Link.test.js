import React from 'react';
import TestRenderer from 'react-test-renderer';
import Link from './Link.js';
import renderer from 'react-test-renderer';
const {act} = TestRenderer;

test('link changes the class when hovered', () => {
  const component = renderer.create(<Link 
        child="This link should change class when you hover it!"/>,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // Trigger the mouseEnter
  act(() => {
    tree.props.onMouseEnter();
  })
  // re-render and test
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();

  // Trigger the mouseLeave
  act(() => {
    tree.props.onMouseLeave();
  })
  // re-render and test
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});