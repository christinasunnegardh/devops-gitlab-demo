import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders the example link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/How to set up a pipeline/i);
  expect(linkElement).toBeInTheDocument();
});