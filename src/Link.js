import React, { useState } from 'react';
import './App.css';

function Link(props) {
  const [linkClass, setLinkClass] = useState("link");
  return (
      <button 
        className={linkClass} 
        onMouseEnter={() => setLinkClass("link test-link-hover")} 
        onMouseLeave={() => setLinkClass("link")}
        onClick={() => props.setShowEasterEgg(!props.showEasterEgg)}>
        {props.child}
      </button>
  );
}

export default Link;
