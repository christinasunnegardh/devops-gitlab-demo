import React, { useState } from 'react';
import Link from "./Link.js"
import './App.css';

function App() {
  const [showEasterEgg, setShowEasterEgg] = useState(false)
  return (
    <div className="App">
      <h1>DevOps Demo</h1>
      <p>How to set up a pipeline (test, build, deploy to Heroku) on GitLab - with this example React application!</p>
      <Link child="This link should change class when you hover it!" 
      setShowEasterEgg={setShowEasterEgg} showEasterEgg={showEasterEgg}/>
      {showEasterEgg && <div class="easter-egg">
          <img src="/easteregg.png" alt="easter egg"/>
          <p>
            Awesome! You found the easter egg! 
            Here I will give a tip on how to edit the 
            code so that it no longer match the current 
            Jest Snapshot, it will thereby not pass the tests.
            You can copy the project and setup the pipeline
            showed in the demo. When you push to the repo,
            the pipeline will fail and not push the "incorrect
            code" to Heroku.</p>
          <p>
            As the tests expect the link to change color at mouseover,
            we can remove that functionality of the applicaton. 
            Remove the highlighted part below.
          </p>
          <img src="/modify-code.png" alt="code"/>
          <p>
            If the code is now pushed, the tests will fail and
            stop this from going into production. By default,
            you will get an email saying that the pipeline failed
            so that you quickly can fix the error.
          </p>
          <p>
            If you wish to read more about the Jest framework, 
            you can read about using it with React applications 
            here: {<a class="link" href="https://jestjs.io/docs/en/tutorial-react">LINK</a>}
          </p>
        </div>}
    </div>
  );
}

export default App;
