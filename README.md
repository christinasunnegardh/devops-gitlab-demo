*Author: Christina Sunnegårdh (sunneg@kth.se)*

This is a sample React application with some simple Jest tests.

This GitLab repo is an accompanying repo for a demo created for the course DD2482 DevOps at KTH.
The contribution can be seen here: [LINK](https://github.com/KTH/devops-course/tree/master/contributions/demo/sunneg). The screencast of this demo can be seen here: [LINK](https://www.youtube.com/watch?v=5IkDrgFSFw4).